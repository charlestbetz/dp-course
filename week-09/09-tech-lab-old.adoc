= Training, education, and frameworks

The lab this week focuses on the topic of frameworks and training, addressing the IT HR and frameworks sections of this week's reading. 

As teams (or individually at your option) you will

* Review a major digital standard, or at least summaries of it
* Compare and contrast that standard to the DPBoK
** This is to be written out
** One format that might be useful is a comparison table, e.g. take a list of topics and evaluate the two standards (e.g. each having a column). 
** Written narrative is also fine. 
* Answer the below questions
** Are frameworks useful? Why or why not?
** If you were a hiring manager, what would you think of DPBoK certfication?
** What would you think of certification in the comparison standard? 
** What changes could the DPBoK make to improve it for your needs?

Here are examples of standards you can choose from. Note that you will in some cases have to rely on third party summaries and descriptions. 

* https://www.pmi.org/disciplined-agile/process/introduction-to-dad[Disciplined Agile Delivery] (now part of PMI)
* https://www.scaledagile.com/[Scaled Agile Framework]
* https://www.isaca.org/bookstore/bookstore-cobit_19-digital/[COBiT] (review "Introduction and Methodology"; it's free but you will need to create a login)
* https://www.scrum.org/[Scrum]
* https://less.works/[Large Scale Scrum (LeSS)]
* https://www.agilealliance.org/agile101/subway-map-to-agile-practices/[Agile Alliance Subway Map]
* ITIL v4 - this is not publicly available, and it may take you significant searching to put together an accurate impression, but you are free to do this


There are other lesser known frameworks. If you come across one, please clear with me first. 

*Alternative.* If you wish, you can focus strictly on the DPBoK itself. Can you identify any gaps? Areas where it is out of date or simply incorrect? If you wish to consider a major contribution to the DPBoK, please let me know. Depending on quality and effort, I might accept such work in lieu of the final. 

NOTE: You can take DPBoK certification at a discount while you are enrolled at St. Thomas. Most semesters, some students opt for this. Contact me if you are interested.

