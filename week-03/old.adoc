
Next, review https://gitlab.com/GoogleCloudPlatform/gitlab-actions/tree/master/example-workflows/gke[this page.] You do NOT do all the steps. You only do some: 

1)  enable the appropriate APIs but DO NOT continue to "Go to Credentials." Go back to https://gitlab.com/google-gitlab-actions/setup-gcloud/tree/master/example-workflows/gke[this page].

2) skip

3) skip

4) Via the provided link, create a service account, recommend calling it `node-svc-k8s`

5) Add `Kubernetes Engine Developer` and `Storage Admin` to the service account

6) Create and download the JSON service account key to a SAFE location. You only get one chance!

7) configure the appropriate secrets in your forked Gitlab repository Settings:

* GKE_PROJECT
* GKE_SA_KEY

NOTE: Keep the service account key in a secure location, such as a password manager. It is equivalent to an ssh private key. Once you put it into Gitlab, you cannot retrieve it. 

8) double check

Assignment: Once you have this configured, the pipeline should run whenever you make a change to server.js and push it to your Gitlab repo (a local change isn't sufficient). Try changing the output of `app.get('/0?'..` as suggested in the code comments. If you prefer, simply change a comment in server.js if you want to start more simply. 

name: Build and Deploy to GKE

on:
  push:
    branches:
    - master

env:
  PROJECT_ID: ${{ secrets.GKE_PROJECT }}
  GKE_CLUSTER:  node-svc-k8s # TODO: update to your cluster name
  GKE_ZONE: us-central1-c   
  DEPLOYMENT_NAME: node-svc-deployment # TODO: update to deployment name
  IMAGE: node-svc

jobs:
  setup-build-publish-deploy:
    name: Setup, Build, Publish, and Deploy
    runs-on: ubuntu-latest

    strategy:
      matrix:
        node-version: [14.x]

    steps:
    - name: Checkout
      uses: actions/checkout@v2

    - name: Use Node.js ${{ matrix.node-version }}
      uses: actions/setup-node@v1
      with:
        node-version: ${{ matrix.node-version }}
    - run: npm ci
    - run: npm run build --if-present
    - run: npm test

    # Setup gcloud CLI
    - uses: GoogleCloudPlatform/github-actions/setup-gcloud@0.1.3
      with:
        service_account_key: ${{ secrets.GKE_SA_KEY }}
        project_id: ${{ secrets.GKE_PROJECT }}

    # Configure Docker to use the gcloud command-line tool as a credential
    # helper for authentication
    - run: |-
        gcloud --quiet auth configure-docker

    # Get the GKE credentials so we can deploy to the cluster
    - run: |-
        gcloud container clusters get-credentials "$GKE_CLUSTER" --zone "$GKE_ZONE"

    # Build the Docker image
    - name: Build
      run: |-
        docker build \
          --tag "gcr.io/$PROJECT_ID/$IMAGE:latest" \
          --build-arg GITHUB_REF="$GITHUB_REF" . 
           

    # Push the Docker image to Google Container Registry
    - name: Publish
      run: |-
        docker push "gcr.io/$PROJECT_ID/$IMAGE:latest"
        
    # Deploy the Docker image to the GKE cluster
    - name: Deploy
      run: |-
        kubectl delete --all pods --namespace=default
        kubectl apply -f deployments.yaml
        kubectl apply -f services.yaml
        kubectl rollout status deployment/$DEPLOYMENT_NAME
        kubectl get services -o wide
