= Lab 08: The service catalog

Stay in your teams from the questions discussion. 

Review https://pubs.opengroup.org/dpbok/standard/chap-invest-mgmt.html#KLP-service-catalog

Review 

* https://blog.invgate.com/creating-an-it-service-catalog
* https://www.bmc.com/blogs/how-to-build-an-it-service-catalog/
* https://gitlab.com/charlestbetz/dp-course/-/blob/master/week-08/FreshService_catalog_examples.pdf
* https://services.howardcountymd.gov/hcportal?id=index_311
* https://cern.service-now.com/service-portal/?id=browse_services

Now, review the Phoenix project and develop a service catalog, using a shared Office 365 Word document. 

Your service catalog should be hierarchical - services grouped into families (2 levels is sufficient). A fragment might look like:

* Infrastructure services
** Compute services
** Storage services

* Security services
** Security consultations

* Software development
** Java
** C++ 

At least 30 items. 

Consider

- technical services
- business services (HR, etc)
- production services (e.g. with an availability SLA)
- requestable services
-- transactional (e.g., provision access or hardware)
-- professional (e.g., consultations)
- potential "platform teams"
- sourcing partners (glance through later Phoenix Project chapters as well)

You can infer the existence of services but make a note if they are inferred. 

What services are troublesome for Parts Unlimited? What seem to be working well? 

Then, we will unite into two teams that each have half the class, and reconcile and discuss. 

Then we will discuss the two catalogs as a class and develop one consensus catalog. 



